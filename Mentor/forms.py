from django import forms
from .models import Mentor
from .models import Video

class MentorEditForm(forms.ModelForm):
    class Meta:
        model = Mentor
        fields = ['username', 'mobile', 'full_name', 'email', 'password', 'gender', 'address', 'aadhar', 'pic', 'class_field', 'section']
        # Customize the fields as needed

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ['title', 'video_file']