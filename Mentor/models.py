from django.db import models

class Mentor(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    username = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15, unique=True)
    full_name = models.CharField(max_length=255)
    email = models.EmailField()
    password = models.CharField(max_length=100)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    address = models.TextField()
    aadhar = models.CharField(max_length=20)
    pic = models.ImageField(upload_to='mentor_pics/')
    class_field = models.CharField(max_length=50)
    section = models.CharField(max_length=10)



class Video(models.Model):
    title = models.CharField(max_length=100)
    video_file = models.FileField(upload_to='videos/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title