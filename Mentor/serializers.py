from rest_framework import serializers
from .models import Mentor

class MentorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mentor
        fields = '__all__'
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate_username(self, value):
        # Check if the username already exists
        if Mentor.objects.filter(username=value).exists():
            raise serializers.ValidationError("This username is already in use.")
        return value

    def validate_mobile(self, value):
        if not value.isdigit() or len(value) != 10:
            raise serializers.ValidationError("Mobile number must be a 10-digit number.")
        
        # Check if the mobile number already exists
        if Mentor.objects.filter(mobile=value).exists():
            raise serializers.ValidationError("This mobile number is already in use.")
        
        return value

    def validate_email(self, value):
        # Check if the email already exists
        if Mentor.objects.filter(email=value).exists():
            raise serializers.ValidationError("This email is already in use.")
        
        # Simple email validation, can be extended for more complex validation
        if '@' not in value or '.' not in value:
            raise serializers.ValidationError("Enter a valid email address.")
        
        return value

    def validate_password(self, value):
        # Assuming a password should be at least 6 characters long
        if len(value) < 6:
            raise serializers.ValidationError("Password must be at least 6 characters long.")
        return value

    def validate_gender(self, value):
        # Assuming gender input should be one of 'M', 'F', or 'O'
        if value not in ['M', 'F', 'O']:
            raise serializers.ValidationError("Invalid gender. Use 'M', 'F', or 'O'.")
        return value

    def validate_address(self, value):
        # Assuming a simple address validation (e.g., should contain alphanumeric characters and spaces)
        # if not value.replace(" ", "").isalnum():
        #     raise serializers.ValidationError("Address must contain alphanumeric characters and spaces only.")
        return value
