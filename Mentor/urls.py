from django.urls import path
from . import views
from .views import MentorListCreateView, MentorDetailView, MentorRegisterView, MentorLoginView, MentorDetailsView

app_name = 'Mentor'

urlpatterns = [
    
    path('mentor/mentors/', MentorListCreateView.as_view(), name='mentor-list'),
    path('mentor/mentors/<int:pk>/', MentorDetailView.as_view(), name='mentor-detail'),
    path('mentor/register/', MentorRegisterView.as_view(), name='mentor-registration'),
    path('mentor/login/', MentorLoginView.as_view(), name='login'),
    path('mentor/user-details/', MentorDetailsView.as_view(), name='user-details'),
    path('', views.home, name='home'),
    path('mentor/edit-profile/', views.edit_profile, name='edit-profile'),
    path('mentor/upload/', views.upload_video, name='upload_video'),
    path('mentor/view-videos/', views.view_videos, name='view_videos'),
    path('mentor/mentor-dashboard/', views.mentor_dashboard, name='mentor-dashboard'),

    # path('registration/', views.mentor_registration, name='mentor-registration'),
    # path('login/', views.mentor_login, name='mentor-login'),
]
