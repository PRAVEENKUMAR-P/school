from rest_framework import generics
from .models import Mentor
from .serializers import MentorSerializer

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate


from django.http import HttpResponse
from django.views import View

from django.contrib.auth.decorators import login_required
from .forms import MentorEditForm

from django.shortcuts import render, redirect
from .forms import VideoForm

from .models import Video


class MentorListCreateView(generics.ListCreateAPIView):
    queryset = Mentor.objects.all()
    serializer_class = MentorSerializer

class MentorDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mentor.objects.all()
    serializer_class = MentorSerializer


class MentorRegisterView(generics.CreateAPIView):
    queryset = Mentor.objects.all()
    serializer_class = MentorSerializer

    def get(self, request, *args, **kwargs):
        return render(request, 'registration.html')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                "status": status.HTTP_201_CREATED,
                "message": "User registered successfully.",
            }, status=status.HTTP_201_CREATED)
        return Response({
            "status": status.HTTP_400_BAD_REQUEST,
            "message": serializer.errors.get('message', 'Invalid input data.'),
            "data": serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)



class MentorLoginView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'login.html')

    def post(self, request, *args, **kwargs):
        username_mobile = request.POST.get('username_mobile')
        password = request.POST.get('password')

        # Authenticate user by username or mobile number
        user = authenticate(request, username=username_mobile, password=password)
        if user is not None:
            # User authentication successful, generate token
            token, created = Token.objects.get_or_create(user=user)
            # return redirect('mentor-dashboard')
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        else:
            # Invalid credentials
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)


class MentorDetailsView(generics.RetrieveAPIView):
    serializer_class = MentorSerializer

    def get_object(self):
        token = self.request.GET.get('token')
        if token:
            try:
                user = Token.objects.get(key=token).user
                return user
            except Token.DoesNotExist:
                return None
        return None

    def get(self, request, *args, **kwargs):
        user = self.get_object()
        if user:
            serializer = self.serializer_class(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Invalid token'}, status=status.HTTP_401_UNAUTHORIZED)
        
@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = MentorEditForm(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('user-details')  # Redirect to the user details page after successful update
    else:
        form = MentorEditForm(instance=request.user)
    
    return render(request, 'edit_profile.html', {'form': form})

@login_required
def upload_video(request):
    if request.method == 'POST':
        form = VideoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('success_page')  # Redirect to a success page after upload
    else:
        form = VideoForm()
    return render(request, 'upload_video.html', {'form': form})

@login_required
def view_videos(request):
    videos = Video.objects.all()
    return render(request, 'view_videos.html', {'videos': videos})


def home(request):
    return render(request, 'home.html')

def mentor_registration(request):
    return render(request, 'Mentor:mentor-registration')

@login_required
def mentor_dashboard(request):
    return render(request, 'mentor_dashboard.html')
