from django.http import HttpResponse
from django.shortcuts import HttpResponsePermanentRedirect
from django.urls import reverse
from django.shortcuts import render, redirect


# def index(request):
#     return HttpResponsePermanentRedirect(reverse('Mentor:home')) 

def home(request):
    return render(request, 'home.html')  