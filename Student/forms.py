from django import forms
from .models import Student
# from .models import Video

class StudentEditForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['username', 'mobile', 'full_name', 'email', 'password', 'gender', 'address', 'aadhar', 'pic', 'class_field', 'section']
        # Customize the fields as needed

# class VideoForm(forms.ModelForm):
#     class Meta:
#         model = Video
#         fields = ['title', 'video_file']