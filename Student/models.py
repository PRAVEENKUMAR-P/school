from django.db import models

class Student(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other'),
    )

    username = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15, unique=True)
    full_name = models.CharField(max_length=255)
    email = models.EmailField()
    password = models.CharField(max_length=100)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    address = models.TextField()
    aadhar = models.CharField(max_length=20)
    pic = models.ImageField(upload_to='student_pics/')
    class_field = models.CharField(max_length=50)
    section = models.CharField(max_length=10)
