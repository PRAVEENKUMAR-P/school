from django.urls import path
from . import views
from .views import (
    StudentRegisterView,
    StudentLoginView,
    StudentDetailsView,
    edit_student_profile,
    view_mentor_videos,
)

app_name = 'Student' 

urlpatterns = [
    path('register/', StudentRegisterView.as_view(), name='student-registration'),
    path('login/', StudentLoginView.as_view(), name='student-login'),
    path('details/', StudentDetailsView.as_view(), name='student-details'),
    path('edit-profile/', edit_student_profile, name='edit-student-profile'),
    path('view-mentor-videos/', view_mentor_videos, name='view-mentor-videos'),
    path('student-dashboard/', views.student_dashboard, name='student-dashboard'),
    # Other paths for student-related functionalities
]
