from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.shortcuts import render, redirect
from .models import Student
from .serializers import StudentSerializer
from Mentor.models import Video as MentorVideo
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.views import View
from .forms import StudentEditForm





class StudentListCreateView(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

class StudentDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentRegisterView(generics.CreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
 
    def get(self, request, *args, **kwargs):
        return render(request, 'student_registration.html')

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({
                "status": status.HTTP_201_CREATED,
                "message": "Student registered successfully.",
            }, status=status.HTTP_201_CREATED)
        return Response({
            "status": status.HTTP_400_BAD_REQUEST,
            "message": serializer.errors.get('message', 'Invalid input data.'),
            "data": serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)

class StudentLoginView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'student_login.html')
 
    def post(self, request, *args, **kwargs):
        username_mobile = request.POST.get('username_mobile')
        password = request.POST.get('password')

        # Authenticate user by username or mobile number
        user = authenticate(request, username=username_mobile, password=password)
        if user is not None:
            # User authentication successful, generate token
            token, created = Token.objects.get_or_create(user=user)
            # return redirect('student-dashboard')
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        else:
            # Invalid credentials
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)

class StudentDetailsView(generics.RetrieveAPIView):
    serializer_class = StudentSerializer

    def get_object(self):
        token = self.request.GET.get('token')
        if token:
            try:
                user = Token.objects.get(key=token).user
                return user
            except Token.DoesNotExist:
                return None
        return None

    def get(self, request, *args, **kwargs):
        user = self.get_object()
        if user:
            serializer = self.serializer_class(user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Invalid token'}, status=status.HTTP_401_UNAUTHORIZED)

@login_required
def edit_student_profile(request):
    if request.method == 'POST':
        form = StudentEditForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('student-details')  # Redirect to the student details page after successful update
    else:
        form = StudentEditForm(instance=request.user)
    
    return render(request, 'edit_student_profile.html', {'form': form})

@login_required
def view_mentor_videos(request):
    mentor_videos = MentorVideo.objects.all()
    return render(request, 'view_mentor_videos.html', {'mentor_videos': mentor_videos})

@login_required
def student_dashboard(request):
    return render(request, 'student_dashboard.html')
